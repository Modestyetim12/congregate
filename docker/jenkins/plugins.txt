cloudbees-folder
antisamy-markup-formatter
workflow-aggregator
build-timeout
credentials-binding
timestamper
ws-cleanup
ant
gradle
github-branch-source
pipeline-github-lib
pipeline-stage-view
git
subversion
ssh-slaves
matrix-auth
pam-auth
ldap
email-ext
mailer
multiple-scms
gitlab-plugin