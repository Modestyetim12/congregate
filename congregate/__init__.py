from gitlab_ps_utils.logger import myLogger
from congregate.helpers.utils import get_congregate_path

log = myLogger(__name__, app_path=get_congregate_path(), log_name='congregate')
